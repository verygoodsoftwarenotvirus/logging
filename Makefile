GOPATH                    := $(GOPATH)
PKG_ROOT                  := gitlab.com/verygoodsoftwarenotvirus/logging
PKG                       := $(PKG_ROOT)/...
ARTIFACTS_DIR             := artifacts
COVERAGE_PATH             := $(ARTIFACTS_DIR)/coverage.out
UNIT_COVERAGE_PATH        := $(ARTIFACTS_DIR)/unit-coverage.out
INTEGRATION_COVERAGE_PATH := $(ARTIFACTS_DIR)/integration-coverage.out

## Go-specific prerequisite stuff
.PHONY: vendor-clean
vendor-clean:
	rm -rf vendor go.sum

vendor:
	if [ ! -f go.mod ]; then go mod init; fi
	go mod vendor

.PHONY: revendor
revendor: vendor-clean vendor

.PHONY: test
test:
	set -ex; go test -v -cover $(PKG)

.PHONY: correct-branch
correct-branch:
ifdef branch
	git tag v$(branch) $(branch)
	git tag -d $(branch)
	git push origin :refs/tags/$(branch)
	git push --tags;
else
	@echo "branch undefined"
endif
