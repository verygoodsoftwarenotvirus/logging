module gitlab.com/verygoodsoftwarenotvirus/logging/v2

go 1.15

require (
	github.com/rs/zerolog v1.15.0
	github.com/sirupsen/logrus v1.4.2
	go.uber.org/zap v1.16.0
)
